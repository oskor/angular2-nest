import { Component } from '@angular/core';
import { MdToolbar } from '@angular2-material/toolbar';
import { MdList } from '@angular2-material/list';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [MdToolbar, MdList]
})
export class AppComponent {
  title = 'app works!';
}
