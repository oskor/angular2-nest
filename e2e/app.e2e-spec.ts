import { Angular2NestPage } from './app.po';

describe('angular2-nest App', function() {
  let page: Angular2NestPage;

  beforeEach(() => {
    page = new Angular2NestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
